package com.than.midterm;

public class Book {
    private String name;
    private int day;
    private int price;

    public Book(String name,int day,int price){
        this.name = name;
        this.day = day;
        this.price = price;
    }

    public String getName(){
        return name;
    }

    public int getDay(){
        return day;
    }
    
    public int getPrice(){
        return price;
    }

    public void rented(){
        System.out.println("You have rented : "+name+".");
        System.out.println("Please return the book wtihin "+day+" days.");
        
    }

    public void returned(){
        System.out.println("You have returned : "+name);
        
    }
    

}
