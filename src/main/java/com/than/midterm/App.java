package com.than.midterm;

import java.util.Scanner;

public class App {

    public static void main(String[] args) {
        Book book1 = new Book("The story of Pinocchio",14,40);
        Book book2 = new Book("Harry potter and sorcerer's stone",14,40);
        Book book3 = new Book("Cryptocurrency 101",14,40);
        Book book4 = new Book("Plant-Based cookbook",14,40);
        Book book5 = new Book("Object Oriented Programming with java",14,40);

        Movie movie1 = new Movie("The Avengers : Endgame",7,50);
        Movie movie2 = new Movie("Avatar",7,50);
        Movie movie3 = new Movie("Transformers 5: The Last Knight",7,50);
        Movie movie4 = new Movie("Fantastic Beasts: The Crimes of Grindelwald",7,50);
        Movie movie5 = new Movie("Fast and Furious 7",7,50);
        Scanner input = new Scanner(System.in);

        while (true) {
            System.out.println("+++++++++++++++++++++++++++++++++");
            System.out.println("Welcome to Book & Movie rent Menu");
            System.out.println("+++++++++++++++++++++++++++++++++");
            System.out.println("1.Rent ");
            System.out.println("2.Return ");
            System.out.println("3.Quit");
            System.out.println("+++++++++++++++++++++++");
            System.out.print("Please Enter Choice: ");
            int choice = input.nextInt();
            System.out.println("+++++++++++++++++++++++");
            switch (choice) {
                case 1:
                    System.out.println("1.Rent a book ");
                    System.out.println("2.Rent a movie ");
                    System.out.println("+++++++++++++++++++++++");
                    System.out.print("Please Enter Choice: ");
                    int choice2 = input.nextInt();
                    System.out.println("+++++++++++++++++++++++");
                    switch (choice2) {
                        case 1:
                            System.out.println("1." + book1.getName() + " (" + book1.getPrice() + " Baht)");
                            ;
                            System.out.println("2." + book2.getName() + " (" + book2.getPrice() + " Baht)");
                            ;
                            System.out.println("3." + book3.getName() + " (" + book3.getPrice() + " Baht)");
                            ;
                            System.out.println("4." + book4.getName() + " (" + book4.getPrice() + " Baht)");
                            ;
                            System.out.println("5." + book5.getName() + " (" + book5.getPrice() + " Baht)");
                            ;
                            System.out.println("++++++++++++++++++++++++++++++++");
                            System.out.print("Please select a book to rent : ");
                            int choice3 = input.nextInt();
                            System.out.println("++++++++++++++++++++++++++++++++");
                            switch (choice3) {
                                case 1:
                                    book1.rented();
                                    break;
                                case 2:
                                    book2.rented();
                                    break;
                                case 3:
                                    book3.rented();
                                    break;
                                case 4:
                                    book4.rented();
                                    break;
                                case 5:
                                    book5.rented();
                                    break;
                                default:
                                    System.out.println("Error : Incorrect choice");
                                    break;
                            }
                            break;
                        case 2:
                            System.out.println("1." + movie1.getName() + " (" + movie1.getPrice() + " Baht)");
                            ;
                            System.out.println("2." + movie2.getName() + " (" + movie1.getPrice() + " Baht)");
                            ;
                            System.out.println("3." + movie3.getName() + " (" + movie1.getPrice() + " Baht)");
                            ;
                            System.out.println("4." + movie4.getName() + " (" + movie1.getPrice() + " Baht)");
                            ;
                            System.out.println("5." + movie5.getName() + " (" + movie1.getPrice() + " Baht)");
                            ;
                            System.out.println("+++++++++++++++++++++++++++++++++");
                            System.out.print("Please select a movie to rent : ");
                            int choice4 = input.nextInt();
                            System.out.println("+++++++++++++++++++++++++++++++++");
                            switch (choice4) {
                                case 1:
                                    movie1.rented();
                                    break;
                                case 2:
                                    movie2.rented();
                                    break;
                                case 3:
                                    movie3.rented();
                                    break;
                                case 4:
                                    movie4.rented();
                                    break;
                                case 5:
                                    movie5.rented();
                                    break;
                                default:
                                    System.out.println("Error : Incorrect choice");
                                    break;
                            }
                            break;
                        default:
                            System.out.println("Error : Incorrect choice");
                            break;
                    }
                    break;
                case 2:
                    System.out.println("1.Renturn a book ");
                    System.out.println("2.Renturn a movie ");
                    System.out.println("+++++++++++++++++++++++");
                    System.out.print("Please Enter Choice: ");
                    int choice5 = input.nextInt();
                    System.out.println("+++++++++++++++++++++++");
                    switch (choice5) {
                        case 1:
                            System.out.println("1." + book1.getName());
                            ;
                            System.out.println("2." + book2.getName());
                            ;
                            System.out.println("3." + book3.getName());
                            ;
                            System.out.println("4." + book4.getName());
                            ;
                            System.out.println("5." + book5.getName());
                            ;
                            System.out.println("++++++++++++++++++++++++++++++++++");
                            System.out.print("Please select a book to return : ");
                            int choice6 = input.nextInt();
                            System.out.println("+++++++++++++++++++++++++++++++++++");
                            switch (choice6) {
                                case 1:
                                    book1.returned();
                                    break;
                                case 2:
                                    book2.returned();
                                    break;
                                case 3:
                                    book3.returned();
                                    break;
                                case 4:
                                    book4.returned();
                                    break;
                                case 5:
                                    book5.returned();
                                    break;
                                default:
                                    System.out.println("Error : Incorrect choice");
                                    break;
                            }
                            break;
                        case 2:
                            System.out.println("1." + movie1.getName());
                            ;
                            System.out.println("2." + movie2.getName());
                            ;
                            System.out.println("3." + movie3.getName());
                            ;
                            System.out.println("4." + movie4.getName());
                            ;
                            System.out.println("5." + movie5.getName());
                            ;
                            System.out.println("+++++++++++++++++++++++++++++++++++");
                            System.out.print("Please select a movie to return : ");
                            int choice7 = input.nextInt();
                            System.out.println("+++++++++++++++++++++++++++++++++++");
                            switch (choice7) {
                                case 1:
                                    movie1.returned();
                                    break;
                                case 2:
                                    movie2.returned();
                                    break;
                                case 3:
                                    movie3.returned();
                                    break;
                                case 4:
                                    movie4.returned();
                                    break;
                                case 5:
                                    movie5.returned();
                                    break;
                                default:
                                    System.out.println("Error : Incorrect choice");
                                    break;
                            }
                        default:
                            System.out.println("Error : Incorrect choice");
                            break;
                    }
                    break;
                case 3:
                    System.out.println("Thank you, please come back again.");
                    System.exit(0);
                default:
                    System.out.println("Error : Incorrect choice");
                    break;
            }
            System.out.println("++++++++++++++++++++++++++++++");
            System.out.println("Back to Main Menu : 1.YES 2.NO");
            System.out.println("++++++++++++++++++++++++++++++");
            System.out.print("Please Enter Choice : ");
            int choice8 = input.nextInt();
            System.out.println("+++++++++++++++++++++++++");
            if (choice8 == 2) {
                System.out.println("Thank you, please come back again.");
                break;
            }
        }
        input.close();
    }
}
